<?php /* Template Name: Contact us */ ?>
<?php get_header(); ?>
<?php 
    global $post;
	$post_slug = $post->post_name;
	$page_subtitle = htmlspecialchars_decode(get_post_meta($post->ID, 'page_metabox_1', true));
?>

	<main role="main" class="page contact-us-page">
		<!-- section -->
		<section class="clean-block clean-form dark <?php echo $post_slug; ?>">

			<div class="container">

				<div class="block-heading">

					<h2 class="text-info"><?php the_title(); ?></h2>
					<p><?php echo $page_subtitle; ?>

				</div>
			
				

					<?php if (have_posts()): while (have_posts()) : the_post(); ?>

						<?php the_content(); ?>

					<?php endwhile; ?>

					<?php endif; ?>

			</div>

		</section>
		<!-- /section -->
	</main>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
