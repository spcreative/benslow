<?php /* Template Name: Home page */ get_header(); ?>
<?php $hero_metabox_title = get_post_meta($post->ID, 'hero_metabox_title', true); ?>
<?php $hero_metabox_subtitle = get_post_meta($post->ID, 'hero_metabox_subtitle', true); ?>
<?php $hero_metabox_button_text = get_post_meta($post->ID, 'buttontext', true); ?>
<?php $hero_metabox_button_link = get_post_meta($post->ID, 'buttonlink', true); ?>
<?php $hero_metabox_hero_colour = get_post_meta($post->ID, 'herocolour', true); ?>
<?php $hero_metabox_hero_image = get_post_meta($post->ID, 'heroimage', true); ?>

	<main role="main" class="page landing-page">

		<section class="clean-block clean-hero" style="background-image: url(<?php echo $hero_metabox_hero_image; ?>);color: <?php echo $hero_metabox_hero_colour; ?>;">
            <div class="text" style="padding-top: 70px;">
				<h2><?php echo $hero_metabox_title; ?></h2>
                <p style="margin: 0px 10px 30px 10px;"><?php echo $hero_metabox_subtitle ?></p><a class="btn btn-outline-light btn-lg" href="<?php echo $hero_metabox_button_link; ?>"><?php echo $hero_metabox_button_text; ?></a></div>
        </section>
		<!-- section -->
		<section>

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php the_content(); ?>

			</article>
			<!-- /article -->

		<?php endwhile; ?>

		<?php else: ?>

		<?php endif; ?>

		</section>
		<!-- /section -->
	</main>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
