			<!-- footer -->
			<footer class="page-footer dark">
				<div class="container">
					<div class="row">
						<div class="col-sm-3">
							<h5>Contact Us</h5>
							<p style="color: rgb(201,201,201);">Nettleden Road<br />Little Gaddesden<br />Hertfordshire<br />HP4 1PL<br /><a href="tel:01442 825 919">01442 825 919<br /></a></p>
						</div>
						<div class="col-sm-3">
							<h5>Services</h5>
							<ul>
								<li><a href="#">Dementia Care</a></li>
								<li><a href="#">Nursing Care</a></li>
								<li><a href="#">Respite Care</a></li>
							</ul>
						</div>
						<div class="col-sm-3">
							<h5>Care Homes</h5>
							<ul>
								<li><a href="#">Highbury Rise</a></li>
								<li><a href="#">Chiltern View</a></li>
								<li><a href="#">Nursing Home</a></li>
								<li><a href="#">Robin Hood House</a></li>
							</ul>
						</div>
						<div class="col-sm-3">
							<h5>Recent News</h5>
							<ul>
								<li><a href="#">News Article 1</a></li>
								<li><a href="#">Benslow wins award</a></li>
								<li><a href="#">Some more news</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="footer-copyright">
					<p>Copyright © 1982 - 2019 Benslow Co. Ltd, St Peter’s House, Market Place, Tring, HP23 5AE<br /></p>
				</div>
			</footer>
			<!-- /footer -->

		</div>
		<!-- /wrapper -->

		<?php wp_footer(); ?>

		<!-- analytics -->
		<script>
		(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
		(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
		l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
		ga('send', 'pageview');
		</script>

	</body>
</html>
