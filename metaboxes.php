<?php

// Homepage metaboxes
add_action( 'add_meta_boxes', 'homepage_metabox' );              
function homepage_metabox() 
{
    global $post;
    if ( 'template-homepage.php' == get_post_meta( $post->ID, '_wp_page_template', true ) ) {
        add_meta_box('homepage_metabox', 'Hero content', 'my_output_function');
    }
}

function my_output_function( $post ) 
{   
    ?><br><label for="hero_metabox_title">Hero title: </label><input type="text" id="hero_metabox_title" name="hero_metabox_title" value="<?php echo get_post_meta($post->ID, 'hero_metabox_title', true); ?>">
    <br><label for="hero_metabox_subtitle">Hero subtitle: </label><input type="text" id="hero_metabox_subtitle" name="hero_metabox_subtitle" value="<?php echo get_post_meta($post->ID, 'hero_metabox_subtitle', true); ?>">
    <br><label for="buttontext">Button text: </label><input type="text" id="buttontext" name="buttontext" value="<?php echo get_post_meta($post->ID, 'buttontext', true); ?>">
    <br><label for="buttonlink">Button link: </label><input type="text" id="buttonlink" name="buttonlink" value="<?php echo get_post_meta($post->ID, 'buttonlink', true); ?>">
    <br><label for="herocolour">Hero colour: </label><input type="text" id="herocolour" name="herocolour" value="<?php echo get_post_meta($post->ID, 'herocolour', true); ?>">
    <br><label for="heroimage">Hero image: </label><input type="text" id="heroimage" name="heroimage" value="<?php echo get_post_meta($post->ID, 'heroimage', true); ?>">
    <?php
}

function save_my_postdata( $post_id ) 
{                   
    if (!empty($_POST['hero_metabox_title']))
    {
        $data1=$_POST['hero_metabox_title'];
        update_post_meta($post_id, 'hero_metabox_title', $data1 );
    }
    if (!empty($_POST['hero_metabox_subtitle']))
    {
        $data2=$_POST['hero_metabox_subtitle'];
        update_post_meta($post_id, 'hero_metabox_subtitle', $data2 );
    }
    if (!empty($_POST['buttontext']))
    {
        $data3=$_POST['buttontext'];
        update_post_meta($post_id, 'buttontext', $data3 );
    }
    if (!empty($_POST['buttonlink']))
    {
        $data4=$_POST['buttonlink'];
        update_post_meta($post_id, 'buttonlink', $data4 );
    }
    if (!empty($_POST['herocolour']))
    {
        $data5=$_POST['herocolour'];
        update_post_meta($post_id, 'herocolour', $data5 );
    }
    if (!empty($_POST['heroimage']))
    {
        $data6=$_POST['heroimage'];
        update_post_meta($post_id, 'heroimage', $data6 );
    }
    if (empty($_POST['hero_metabox_title']))
    {
        delete_post_meta($post_id, 'hero_metabox_title');
    }
    if (empty($_POST['hero_metabox_subtitle']))
    {
        delete_post_meta($post_id, 'hero_metabox_subtitle');
    }
    if (empty($_POST['buttontext']))
    {
        delete_post_meta($post_id, 'buttontext');
    }
    if (empty($_POST['buttonlink']))
    {
        delete_post_meta($post_id, 'buttonlink');
    }
    if (empty($_POST['herocolour']))
    {
        delete_post_meta($post_id, 'herocolour');
    }
    if (empty($_POST['heroimage']))
    {
        delete_post_meta($post_id, 'heroimage');
    }
}
add_action( 'save_post', 'save_my_postdata' );

// Page metaboxes
add_action( 'add_meta_boxes', 'page_metabox' );              
function page_metabox() 
{
    add_meta_box('page_metabox', 'Page subtitle', 'vehicle_specs_metabox', 'page');
}

function vehicle_specs_metabox( $post ) 
{
    $page_metabox_1 = get_post_meta($_GET['post'], 'page_metabox_1' , true );
    wp_editor( htmlspecialchars_decode($page_metabox_1), 'page_subtitle', $settings = array('textarea_name'=>'pagesubtitle') );
}

function save_page_metabox( $post_id ) 
{                   
    if (!empty($_POST['pagesubtitle']))
    {
        $data1=htmlspecialchars($_POST['pagesubtitle']);
        update_post_meta($post_id, 'page_metabox_1', $data1 );
    }
    if (empty($_POST['pagesubtitle']))
    {
        delete_post_meta($post_id, 'page_metabox_1');
    }
}
add_action( 'save_post', 'save_page_metabox' );

?>