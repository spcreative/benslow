<?php get_header(); ?>
<?php 
    global $post;
	$post_slug = $post->post_name;
	$page_subtitle = htmlspecialchars_decode(get_post_meta($post->ID, 'page_metabox_1', true));
?>

	<main role="main" class="page">
		<!-- section -->
		<section class="clean-block <?php echo $post_slug; ?>">

			<div class="container">

				<div class="block-heading">

					<h2 class="text-info"><?php the_title(); ?></h2>
					<p><?php echo $page_subtitle; ?>

				</div>
			
				<div class="row justify-content-center">

					<?php if (have_posts()): while (have_posts()) : the_post(); ?>

						<?php the_content(); ?>

					<?php endwhile; ?>

					<?php endif; ?>

				</div>

			</div>

		</section>
		<!-- /section -->
	</main>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
