<?php

if (function_exists('add_theme_support'))
{
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size('custom-size', 700, 200, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');

    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Add custom logo support
    function custom_logo_setup() {
        $defaults = array(
        'height'      => 80,
        'width'       => 205,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => array( 'site-title', 'site-description' ),
        );
        add_theme_support( 'custom-logo', $defaults );
    }
}

// Load scripts (header.php)
function blank_header_scripts()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {
        wp_register_script('jquery', get_template_directory_uri() . '/js/jquery.min.js', array('jquery'), '1.0.0'); // Custom scripts
        wp_enqueue_script('jquery'); // Enqueue it!

        wp_register_script('smoothproducts', get_template_directory_uri() . '/js/smoothproducts.min.js', array('jquery'), '1.0.0'); // Custom scripts
        wp_enqueue_script('smoothproducts'); // Enqueue it!

        wp_register_script('bootstrap', get_template_directory_uri() . '/bootstrap/js/bootstrap.min.js', array('jquery'), '4.3.1'); // Custom scripts
        wp_enqueue_script('bootstrap'); // Enqueue it!
    }
}

// Load styles
function custom_styles()
{
    wp_register_style('benslow', get_template_directory_uri() . '/style.css', array(), '1.0', 'all');
    wp_enqueue_style('benslow'); // Enqueue it!

    wp_register_style('bootstrap', get_template_directory_uri() . '/bootstrap/css/bootstrap.min.css', array(), '4.3.1', 'all');
    wp_enqueue_style('bootstrap'); // Enqueue it!

    wp_register_style('baguettebox', 'https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css');
    wp_enqueue_style('baguettebox'); // Enqueue it!

    wp_register_style('googlefonts', 'https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i');
    wp_enqueue_style('googlefonts'); // Enqueue it!
}

// Register navigation

function header_nav() {
    wp_nav_menu(
        array(
            'theme_location'  => 'navbar-menu', 
            'container'       => 'div',
            'container_class' => 'collapse navbar-collapse',
            'container_id'    => 'navcol-1',
            'menu_class'      => 'nav navbar-nav ml-auto',
            'items_wrap'      => '<ul class="nav navbar-nav ml-auto">%3$s</ul>',
            'add_li_class'    => 'nav-item'
        )
    );
}

function footer_nav() {
    wp_nav_menu(
        array(
            'theme_location'  => 'footer-menu',
            'container'       => 'div'
        )
    );
}

function register_menus() {
    register_nav_menus(
        array(
            'navbar-menu' => __('Navbar menu', 'header_nav'),
            'footer-menu' => __('Footer menu', 'footer_nav')
        )
    );  
}

// If Dynamic Sidebar Exists
if (function_exists('register_sidebar'))
{
    // Define Sidebar Widget Area 1
    register_sidebar(array(
        'name' => __('Widget Area 1', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-1',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));

    // Define Sidebar Widget Area 2
    register_sidebar(array(
        'name' => __('Widget Area 2', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-2',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));
}

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function html5wp_pagination()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
}

// Custom View Article link to Post
function html5_blank_view_article($more)
{
    global $post;
    return '... <a class="view-article" href="' . get_permalink($post->ID) . '">' . __('View Article', 'html5blank') . '</a>';
}

function custom_class_menu_nav_link ( $atts, $item, $args ) {
    $class = 'nav-link';
    $atts['class'] = $class;
    return $atts;
}

function custom_class_current_menu_link ($classes, $item) {
    if (in_array('current-menu-item', $classes) ){
        $classes[] = 'active ';
    }
    return $classes;
}
 
function hide_editor() {
    if (isset($_GET['post'])) {
        $id = $_GET['post'];
        $template = get_post_meta($id, '_wp_page_template', true);
        switch ($template) {
            case 'template-homepage.php':
            // the below removes 'editor' support for 'pages'
            // if you want to remove for posts or custom post types as well
            // add this line for posts:
            // remove_post_type_support('post', 'editor');
            // add this line for custom post types and replace 
            // custom-post-type-name with the name of post type:
            // remove_post_type_support('custom-post-type-name', 'editor');
            remove_post_type_support('page', 'editor');
            break;
            default :
            // Don't remove any other template.
            break;
        }
    }
}

function add_navitem_class_on_li($classes, $item, $args) {
    $classes[] = 'nav-item';
    return $classes;
  }

  function services_menu_classes($menu) {
    global $post;
    if (get_post_type($post) == 'services' && $_SERVER['REQUEST_URI'] != '/' ) {
        $menu = str_replace( 'current-menu-item', '', $menu ); 
        $menu = str_replace( 'menu-item-26', 'menu-item-26 current-menu-item active', $menu ); // add the current_page_parent class to the page you want
    }
    return $menu;
}

/*------------------------------------*\
	Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions
add_action('wp_enqueue_scripts', 'custom_styles'); // Add Theme Stylesheet
add_action('init', 'register_menus'); // Add custom menu
add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination
# add_action( 'admin_init', 'hide_editor' ); // Hide editor for specified templates
add_action( 'after_setup_theme', 'custom_logo_setup' ); // Add custom logo to theme customiser
add_action('init', 'services_post_type'); // Add services custom post type
add_action('init', 'homes_post_type'); // Add homes custom post type

// Remove Actions
/*remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);*/

// Add Filters
add_filter('excerpt_more', 'html5_blank_view_article'); // Add 'View Article' button instead of [...] for Excerpts
add_filter('nav_menu_link_attributes', 'custom_class_menu_nav_link', 10, 3 ); // Add class to link in menu
add_filter('nav_menu_css_class' , 'custom_class_current_menu_link' , 10 , 2); // Add active class to current menu item
add_filter('show_admin_bar', '__return_false'); // Remove Admin bar
add_filter('nav_menu_css_class','add_navitem_class_on_li',1,3);
add_filter( 'nav_menu_css_class', 'services_menu_classes', 10,2 ); // Correct menu classes for services

// Metaboxes
require ('metaboxes.php');

// Custom post types
require ('post-types.php');
?>