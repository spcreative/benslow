<?php

function services_post_type()
{
    register_post_type('services', 
        array(
        'labels' => array(
            'name' => __('Services'),
            'singular_name' => __('Services'),
            'add_new' => __('Add new'),
            'add_new_item' => __('Add New service'),
            'edit' => __('Edit'),
            'edit_item' => __('Edit service'),
            'new_item' => __('New service'),
            'view' => __('View services'),
            'view_item' => __('View service'),
            'search_items' => __('Search services'),
            'not_found' => __('No services found'),
            'not_found_in_trash' => __('No services found in trash')
        ),
        'public' => true,
        'menu_icon' => 'dashicons-clipboard',
        'hierarchical' => true,
        'has_archive' => true,
        'show_in_rest' => true,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail'
        )
    ));
}

function homes_post_type()
{
    register_post_type('homes', 
        array(
        'labels' => array(
            'name' => __('Homes'),
            'singular_name' => __('Homes'),
            'add_new' => __('Add new'),
            'add_new_item' => __('Add New home'),
            'edit' => __('Edit'),
            'edit_item' => __('Edit home'),
            'new_item' => __('New home'),
            'view' => __('View homes'),
            'view_item' => __('View home'),
            'search_items' => __('Search homes'),
            'not_found' => __('No homes found'),
            'not_found_in_trash' => __('No homes found in trash')
        ),
        'public' => true,
        'menu_icon' => 'dashicons-admin-home',
        'hierarchical' => true,
        'has_archive' => true,
        'show_in_rest' => true,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail'
        )
    ));
}

?>